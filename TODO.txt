
1- Lista de pacientes e Login

(INPROVE)
Criar um login page
	Melhorias : 
		- Ter Password
		- Guardar User em algum lado

(DONE)
Criar um objecto que guarde a informa��o de um paciente (Nome, data de nascimento, foto, morada, patologia).

(IMPROVE)
Criar 10 pacientes me lista com o layout no link em baixo a serem carregados.
	Melhorias :
		- Aperfei�oar Layout

(TODO)
Ao clicar no cart�o do paciente deveria abrir um popup com a info detalhada (Nome, data de nascimento, patologia). Neste caso n�o ser� necess�rio aplicar especifica��es de design.


2- Usar Shared Preferences ou Settings do Android/Sistema de Ficheiros

(WIP)
Guardar a informa��o de cada paciente num ficheiro de configura��o  em shared preferences e a foto em ficheiros.
	Todo:
		-Guardar foto em ficheiros


3- Liga��o a um servidor

(DONE)
No bot�o settings da lista de pacientes (�cone roda dentada), ao clicar deveria abrir um popup onde possa guardar o url do servidor e poder alter�-lo (n�o � necess�rio aplicar especifica��es de design).

(TODO)
Fazer um httpConnection, obter a reposta do servidor e apresenta��o da lista de pacientes caso a conex�o seja bem sucedida e os dados sejam carregados para a List View.

(TODO)
Poder�s escolher um servi�o dispon�vel na internet que ofere�a uma resposta JSON que dever� fazer parse (esta informa��o n�o � importante para a lista de pacientes).
Se o servi�o falhar dever� dar informa��o ao utilizador (n�o � necess�rio aplicar especifica��es de design).

(TODO)
4- Relat�rio

http://ramsandroid4all.blogspot.com/2016/08/httpurlconnection-with-json-parsing.html