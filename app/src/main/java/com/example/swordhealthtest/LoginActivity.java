package com.example.swordhealthtest;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class LoginActivity extends Activity {

    EditText username;
    Button login;
    TextInputLayout textInputLayout;

    //TODO: save user data

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);
        username=findViewById(R.id.username);
        login=findViewById(R.id.login);
        textInputLayout = findViewById(R.id.input_layout);

        login.setOnClickListener(new View.OnClickListener() {

            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View view) {

                if(!username.getText().toString().isEmpty())
                {
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    intent.putExtra("USERNAME", username.getText().toString());
                    startActivity(intent);
                }else
                {
                    textInputLayout.setError("You need to enter a name");
                }
            }
        });
    }
}
