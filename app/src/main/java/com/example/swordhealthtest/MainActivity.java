package com.example.swordhealthtest;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends Activity {

    TextView username, patientsCount;
    RecyclerView recyclerView;
    ImageButton settingsButton;
    CircleImageView userPhoto;
    ProgressDialog progressDialog;

    private ArrayList<Patient> mPatients = new ArrayList<>();
    private static final int numColumns = 4;

    SharedPreferencesHelper mPreferencesHelper;


    PatitentsListAdapter patientsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        mPreferencesHelper = new SharedPreferencesHelper(PreferenceManager.getDefaultSharedPreferences(this));

        initRecyclerView();

        getPatientsData();

        populateHeader();



    }

    @Override
    protected void onDestroy() {
        mPreferencesHelper.savePatientsInfo(mPatients);
        super.onDestroy();
    }



    public void populateHeader(){

        Typeface avenirMedium = Typeface.createFromAsset(getAssets(),"fonts/avenir/AvenirLTStd-Medium.otf");

        userPhoto = findViewById(R.id.userPhoto);

        RequestOptions options = new RequestOptions()
                .placeholder(R.drawable.ic_launcher_background);
        Glide.with(this)
                .asBitmap()
                .load("https://www.w3schools.com/howto/img_avatar.png")
                .apply(options)
                .into(userPhoto);

        username = findViewById(R.id.userName);
        username.setText(getString(R.string.hello_username, getIntent().getStringExtra("USERNAME")));
        username.setTypeface(avenirMedium);

        patientsCount = findViewById(R.id.patientsCount);
        patientsCount.setTypeface(avenirMedium);
        updatePatientsCount();

        settingsButton = findViewById(R.id.settingsButton);
        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showServerDialog();
            }
        });
    }

    private void updatePatientsCount() {
        patientsCount.setText(getString(R.string.header_patients_count, mPatients.size()));
    }

    private void getPatientsData() {

        ArrayList<Patient> mNewPatients = mPreferencesHelper.getPatientsInfo();

        if (mNewPatients.size() > 0) {

            mPatients.addAll(mNewPatients);
            patientsAdapter.notifyDataSetChanged();

        } else {
            getDummyData();
        }
    }

    private void getDummyData() {

        mPatients.add(new Patient("Brock Lee",35,"https://1ofdmq2n8tc36m6i46scovo2e-wpengine.netdna-ssl.com/wp-content/uploads/2014/04/Steven_Hallam-slide.jpg","455 Berkshire Street", "pathology 1"));
        mPatients.add(new Patient("Barb Ackue",75,"https://fedspendingtransparency.github.io/assets/img/user_personas/repurposer_mug.jpg","Dracut, MA 01826", "pathology 2"));
        mPatients.add(new Patient("Phil Harmonic ",45,"https://developers.google.com/web/images/contributors/philipwalton.jpg?hl=pt-br","366 Vine St.", "pathology 3"));
        mPatients.add(new Patient("Buck Kinnear",23,"https://febratextil.fcem.com.br/uploads/palestrantes/team1.jpg","Gibsonia, PA 15044", "pathology 4"));
        mPatients.add(new Patient("Sal Monella",67,"https://pbs.twimg.com/profile_images/986804261991268353/s9Mmt4ao_200x200.jpg","55 Swanson Street", "pathology 5"));
        mPatients.add(new Patient("Nick R. Bocker",47,"https://febratextil.fcem.com.br/uploads/palestrantes/team4.jpg","Sioux Falls, SD 57103", "pathology 6"));
        mPatients.add(new Patient("Jimmy Changa",54,"https://www.portraitprofessional.com/img/page-images/homepage/v18/slider/layers-B.jpg","502 W. Albany Street", "pathology 7"));
        mPatients.add(new Patient("Saul T. Balls",46,"https://www.hotfootdesign.co.uk/wp-content/uploads/2016/05/d5jA8OZv.jpg","Spartanburg, SC 29301", "pathology 8"));
        mPatients.add(new Patient("Bill Emia",42,"http://movimentoux.com/_assets/img/19-foto-paola-sales.jpg?v=1556468006571","860 N. Corona Street", "pathology 9"));
        mPatients.add(new Patient("Holly Graham",45,"https://userbrain.net/static/foto-stefan-a65d9944ae98045d12d71c8db365e6ec.jpg","Rolla, MO 65401", "pathology 10"));


        int pos = 0;
        for (final Patient p:
                mPatients
             ) {

            final int position = pos;

            Glide.with(this)
                    .asBitmap()
                    .load(p.getPhoto())
                    .listener(new RequestListener<Bitmap>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
                            mPatients.get(position).setPhoto(Utils.saveImage(resource, p.getName()));
                            patientsAdapter.notifyItemChanged(position);
                            return false;
                        }
                    }).submit();

            pos++;
        }
    }

    private void initRecyclerView() {
        recyclerView = findViewById(R.id.recyclerView);
        patientsAdapter = new PatitentsListAdapter(this, mPatients);

        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(numColumns, LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setAdapter(patientsAdapter);
    }

    private void showServerDialog() {

        // get prompts.xml view
        LayoutInflater layoutInflater = LayoutInflater.from(MainActivity.this);
        View promptView = layoutInflater.inflate(R.layout.server_dialog, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
        alertDialogBuilder.setView(promptView);

        final EditText editText = promptView.findViewById(R.id.edittext);
        // setup a dialog window
        alertDialogBuilder.setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if(Utils.verifyConnection(MainActivity.this)) {
                            ServerConnection myAsync = new ServerConnection(editText.getText().toString(), new AsyncResponse() {

                                @Override
                                public void connectionFailed() {
                                    progressDialog.dismiss();
                                    AlertDialog.Builder builder = new AlertDialog.
                                            Builder(MainActivity.this);
                                    builder.setTitle("Failed!");
                                    builder.setMessage("Server connection failed");
                                    builder.setPositiveButton("OK",
                                            new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    dialogInterface.cancel();
                                                }
                                            });
                                    builder.create().show();
                                }

                                @Override
                                public void onPreExecute() {
                                    progressDialog = ProgressDialog.show(MainActivity.this, "downloading", "please wait");
                                }

                                @Override
                                public void onSuccess(ArrayList<Patient> mNewPatients) {
                                    progressDialog.dismiss();
                                    mPatients.clear();
                                    mPatients.addAll(mNewPatients);
                                    patientsAdapter.notifyDataSetChanged();
                                    updatePatientsCount();
                                }
                            });
                            myAsync.execute();
                        }
                    }
                })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // create an alert dialog
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    public interface AsyncResponse {
        void connectionFailed();

        void onPreExecute();

        void onSuccess(ArrayList<Patient> mNewPatients);
    }


    static class ServerConnection extends AsyncTask<Void, Void, String> {


        String serverURL;

        AsyncResponse response;

        ServerConnection(String url, AsyncResponse asyncResponse) {
            super();
            serverURL = url;
            response = asyncResponse;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            response.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... voids) {
            String result = null;
            try {
                URL url = new URL(serverURL);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                InputStream in = new BufferedInputStream(urlConnection.getInputStream());

                result = inputStreamToString(in);


            } catch (Exception e) {
                e.printStackTrace();

            }
            return result;
        }

        private String inputStreamToString(InputStream is) {
            String rLine;
            StringBuilder answer = new StringBuilder();

            InputStreamReader isr = new InputStreamReader(is);

            BufferedReader rd = new BufferedReader(isr);

            try {
                while ((rLine = rd.readLine()) != null) {
                    answer.append(rLine);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return answer.toString();
        }

        @Override
        protected void onPostExecute(String s) {



            if(s == null) {
                response.connectionFailed();
            }else {

                super.onPostExecute(s);

                try {
                    JSONObject jsonObject = new JSONObject(s);
                    JSONArray jsonArray = jsonObject.getJSONArray("contacts");
                    ArrayList<Patient> mNewPatients = new ArrayList<>();
                    for (int i = 0; i < jsonArray.length(); i++) {

                        mNewPatients.add(new Patient(jsonArray.getJSONObject(i).getString("name"),
                                50,                                                 //dummy age value
                                "https://www.w3schools.com/howto/img_avatar.png",   //dummy photo url
                                jsonArray.getJSONObject(i).getString("address"),
                                "PATHOLOGY A"                                   //dummy pathology value
                        ));

                    }

                    response.onSuccess(mNewPatients);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }
    }
}


