package com.example.swordhealthtest;

public class Patient {

    //TODO: Age pass to birthday

    private String name;
    private int age;
    private String photo;
    private String address;
    private String pathology;

    Patient(final String name, int age, String photo, String address, String pathology) {
        this.name = name;
        this.age = age;
        this.photo = photo;
        this.address = address;
        this.pathology = pathology;

    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    int getAge() {
        return age;
    }

    String getPhoto() {
        return photo;
    }

    void setPhoto(String photo) {
        this.photo = photo;
    }

    String getAddress() {
        return address;
    }


    String getPathology() {
        return pathology;
    }

}
