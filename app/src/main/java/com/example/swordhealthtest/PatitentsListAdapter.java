package com.example.swordhealthtest;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class PatitentsListAdapter extends RecyclerView.Adapter<PatitentsListAdapter.ViewHolder>{

    private Context mContext;
    private ArrayList<Patient> mPatients;

    PatitentsListAdapter(Context mContext, ArrayList<Patient> mPatients) {
        this.mContext = mContext;
        this.mPatients = mPatients;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_patient, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {

        //patient photo
        RequestOptions options = new RequestOptions()
                .placeholder(R.drawable.ic_launcher_background);


        Glide.with(mContext)
                .asBitmap()
                .load(mPatients.get(i).getPhoto())
                .apply(options)
                .into(viewHolder.photo);

        //patient name
        viewHolder.name.setText(mPatients.get(i).getName());

        //patient age
        viewHolder.age.setText(mContext.getString(R.string.patient_age, mPatients.get(i).getAge()));

        viewHolder.patient_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPatientDialog(mPatients.get(i));
            }
        });



    }

    private void showPatientDialog(Patient p) {
        final Dialog patientDialog = new Dialog(mContext);
        patientDialog.setContentView(R.layout.patient_popup);

        TextView patientName , patientAge, patientInfo, close ;
        CircleImageView patientPhoto;

        close = patientDialog.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                patientDialog.dismiss();
            }
        });

        patientPhoto = patientDialog.findViewById(R.id.photo);
        RequestOptions options = new RequestOptions()
                .placeholder(R.drawable.ic_launcher_background);

        Glide.with(mContext)
                .asBitmap()
                .load(p.getPhoto())
                .apply(options)
                .into(patientPhoto);

        patientName = patientDialog.findViewById(R.id.name);
        patientName.setText(p.getName());

        patientAge = patientDialog.findViewById(R.id.age);
        patientAge.setText(mContext.getString(R.string.patient_age, p.getAge()));

        patientInfo = patientDialog.findViewById(R.id.info);
        patientInfo.setText(mContext.getString(R.string.patient_info, p.getPathology(), p.getAddress()));

        patientDialog.show();
    }


    @Override
    public int getItemCount() {

        return mPatients.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        CircleImageView photo;
        TextView name, age;
        ConstraintLayout patient_card;

        ViewHolder(@NonNull View itemView) {
            super(itemView);

            Typeface avenirMedium = Typeface.createFromAsset(mContext.getAssets(),"fonts/avenir/AvenirLTStd-Medium.otf");
            Typeface avenirBookOblique = Typeface.createFromAsset(mContext.getAssets(),"fonts/avenir/AvenirLTStd-BookOblique.otf");

            photo = itemView.findViewById(R.id.photo);
            name = itemView.findViewById(R.id.name);
            name.setTypeface(avenirMedium);
            age = itemView.findViewById(R.id.age);
            age.setTypeface(avenirBookOblique);

            patient_card = itemView.findViewById(R.id.patient_card);
        }
    }
}
