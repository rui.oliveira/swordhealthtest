package com.example.swordhealthtest;

import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

class SharedPreferencesHelper {

    private final SharedPreferences mSharedPreferences;
    static final String patientsDataKey = "PATIENTS_DATA";

    SharedPreferencesHelper(SharedPreferences mSharedPreferences) {
        this.mSharedPreferences = mSharedPreferences;
    }

    boolean savePatientsInfo(List<Patient> mPatients){

        SharedPreferences.Editor mEditor;
        String patientsJSONString;

        mEditor = mSharedPreferences.edit();
        patientsJSONString = new Gson().toJson(mPatients);
        mEditor.putString(patientsDataKey, patientsJSONString);
        mEditor.apply();
        return mEditor.commit();
    }

    ArrayList<Patient> getPatientsInfo() {
        ArrayList<Patient> mNewPatients;
        String patientsJSONString = mSharedPreferences.getString(patientsDataKey,null);
        if(patientsJSONString != null) {
            Type type = new TypeToken<ArrayList<Patient>>() {
            }.getType();
            mNewPatients = new Gson().fromJson(patientsJSONString, type);
        }else
            mNewPatients= new ArrayList<Patient>();
        return mNewPatients;
    }
}
