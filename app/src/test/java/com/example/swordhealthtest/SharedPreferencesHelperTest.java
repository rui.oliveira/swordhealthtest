package com.example.swordhealthtest;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.*;
import static org.mockito.Mockito.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import java.util.ArrayList;


@RunWith(MockitoJUnitRunner.class)
public class SharedPreferencesHelperTest {
    
    
    private Patient patientTest1 = new Patient("Name Test 1", 1,"test/photo/path1","Address Test 1", "pathology test 1");
    private Patient patientTest2 = new Patient("Name Test 2", 2,"test/photo/path2","Address Test 2", "pathology test 2");
    private ArrayList<Patient> patientArrayList;
    private SharedPreferencesHelper mMockSharedPreferencesHelper;
    private SharedPreferencesHelper mMockBrokenSharedPreferencesHelper;
    @Mock
    SharedPreferences mMockSharedPreferences;
    @Mock
    SharedPreferences mMockBrokenSharedPreferences;
    @Mock
    SharedPreferences.Editor mMockEditor;
    @Mock
    SharedPreferences.Editor mMockBrokenEditor;
    @Before
    public void initMocks() {
        // Create patientArrayList to persist.
        patientArrayList = new ArrayList<Patient>();
        patientArrayList.add(patientTest1);
        patientArrayList.add(patientTest2);
        // Create a mocked SharedPreferences.
        mMockSharedPreferencesHelper = createMockSharedPreference();
        // Create a mocked SharedPreferences that fails at saving data.
        mMockBrokenSharedPreferencesHelper = createBrokenMockSharedPreference();
    }
    @Test
    public void sharedPreferencesHelper_SaveAndReadPatientsInformation() {
        // Save the patients information to SharedPreferences
        boolean success = mMockSharedPreferencesHelper.savePatientsInfo(patientArrayList);
        assertThat("Checking that PatientsList.save... returns true",
                success, is(true));
        // Read patients information from SharedPreferences
        ArrayList<Patient> savedSharedPreferenceEntry =
                mMockSharedPreferencesHelper.getPatientsInfo();
        // Make sure both written and retrieved patients information are equal.
        assertThat("Checking that patientTest1 addres has been persisted and read correctly",
                patientArrayList.get(0).getAddress(),
                is(equalTo(savedSharedPreferenceEntry.get(0).getAddress())));
        assertThat("Checking that patientTest1 age has been persisted and read correctly",
                patientArrayList.get(0).getAge(),
                is(equalTo(savedSharedPreferenceEntry.get(0).getAge())));
        assertThat("Checking that patientTest1 name has been persisted and read correctly",
                patientArrayList.get(0).getName(),
                is(equalTo(savedSharedPreferenceEntry.get(0).getName())));
        assertThat("Checking that patientTest1 pathology has been persisted and read correctly",
                patientArrayList.get(0).getPathology(),
                is(equalTo(savedSharedPreferenceEntry.get(0).getPathology())));
        assertThat("Checking that patientTest1 photo has been persisted and read correctly",
                patientArrayList.get(0).getPhoto(),
                is(equalTo(savedSharedPreferenceEntry.get(0).getPhoto())));


        assertThat("Checking that patientTest2 address has been persisted and read correctly",
                patientArrayList.get(1).getAddress(),
                is(equalTo(savedSharedPreferenceEntry.get(1).getAddress())));
        assertThat("Checking that patientTest2 age has been persisted and read correctly",
                patientArrayList.get(1).getAge(),
                is(equalTo(savedSharedPreferenceEntry.get(1).getAge())));
        assertThat("Checking that patientTest2 name has been persisted and read correctly",
                patientArrayList.get(1).getName(),
                is(equalTo(savedSharedPreferenceEntry.get(1).getName())));
        assertThat("Checking that patientTest2 pathology has been persisted and read correctly",
                patientArrayList.get(1).getPathology(),
                is(equalTo(savedSharedPreferenceEntry.get(1).getPathology())));
        assertThat("Checking that patientTest2 photo has been persisted and read correctly",
                patientArrayList.get(1).getPhoto(),
                is(equalTo(savedSharedPreferenceEntry.get(1).getPhoto())));
    }
    @Test
    public void sharedPreferencesHelper_SavePatientsInformationFailed_ReturnsFalse() {
        // Read patients information from a broken SharedPreferencesHelper
        boolean success =
                mMockBrokenSharedPreferencesHelper.savePatientsInfo(patientArrayList);
        assertThat("Makes sure writing to a broken SharedPreferencesHelper returns false", success,
                is(false));
    }
    /**
     * Creates a mocked SharedPreferences.
     */
    private SharedPreferencesHelper createMockSharedPreference() {
        // Mocking reading the SharedPreferences as if mMockSharedPreferences was previously written
        // correctly.
        when(mMockSharedPreferences.getString(eq(SharedPreferencesHelper.patientsDataKey), anyString()))
                .thenReturn(new Gson().toJson(patientArrayList));
        // Mocking a successful commit.
        when(mMockEditor.commit()).thenReturn(true);
        // Return the MockEditor when requesting it.
        when(mMockSharedPreferences.edit()).thenReturn(mMockEditor);
        return new SharedPreferencesHelper(mMockSharedPreferences);
    }
    /**
     * Creates a mocked SharedPreferences that fails when writing.
     */
    private SharedPreferencesHelper createBrokenMockSharedPreference() {
        // Mocking a commit that fails.
        when(mMockBrokenEditor.commit()).thenReturn(false);
        // Return the broken MockEditor when requesting it.
        when(mMockBrokenSharedPreferences.edit()).thenReturn(mMockBrokenEditor);
        return new SharedPreferencesHelper(mMockBrokenSharedPreferences);
    }
}
